import asyncio, aiohttp, requests, re, os, time
import concurrent.futures
from aiohttp import web
from operator import itemgetter

#url = "https://en.wikipedia.org/w/api.php?action=query&prop=extracts&explaintext=True&exsectionformat=plain&format=xml&exlimit=max&titles="
url = "https://en.wikipedia.org/w/api.php?action=query&prop=revisions&rvprop=content&rvslots=main&format=xml&titles="
markups = { "xml" : (r"</?", r".*?>") }
qcount = 0
wcount = 0

async def parallelfetch(url, querylist): # the wiki apis allow for 50 titles (pages) at a time
    global serialc
    global qcount
    global wcount
    chunksno = len(querylist) // 50    # num of complete 50-packs
    qcount += len(querylist)
    allchunks = [querylist[chno*50:(chno+1)*50] for chno in range(0,chunksno)]
    lastchunk = allchunks.append(querylist[(chunksno*50):(chunksno*50)+(len(querylist) % 50)]) #remainder, e.g. 100-198
    atp = [("{}|" * len(fpack)).format(*fpack) for fpack in allchunks]
    results = []
    for querystring in atp:
        async with aiohttp.ClientSession() as session:
            async with session.get(f'{url}{querystring}') as resp:
                thetext = await resp.text()
                wcount += len(thetext)
                results.append(thetext)
    return results, qcount, wcount


async def serialfetch(url, querylist): # the wiki apis allow for 50 titles (pages) at a time
    global serialc
    results = []
    for query in querylist:
        print(f"\rgetting No {serialc}:" + query)
        serialc += 1
        async with aiohttp.ClientSession() as session:
            async with session.get(f'{url}{query}') as resp:
                results.append(await resp.text())
    return results, serialc


async def scrape(texts, internalmarkup, markup, region):
    scrapedtexts = []
    for text in texts:
        try:
            text = re.sub("\[\[.*\|", "", text)
            words = re.split('(\. |\\n|\s|[|])', text)
            while not re.search(markups[markup][0]+region+markups[markup][1], words[0]): # eliminate forward (header boilerplate)
                words = words[1:]
            while not re.search(markups[markup][0]+region+markups[markup][1], words[-1]): # eliminate backwards from the end (footer)
                words = words[:-1]
            words = [re.sub("[\W\|]", "", w) for w in words if bool(w.strip())]
        except:
            words = "### COULD NOT PARSE ###"
        scrapedtexts.append([w for w in words if not w.isspace()])
    return scrapedtexts

