import parsetheweb, re, asyncio, aiohttp, requests
from parsetheweb import *

with open("./resources/adjectives.txt") as f:
    adjectives = re.split("\n", f.read())

with open("./resources/nouns.txt") as f:
    nouns = re.split("\n", f.read())        


S = requests.Session()
URL = "https://en.wikipedia.org/w/api.php"
PARAMS = {
    "action": "query",
    "format": "json",
    "list": "random",
#    "rnlimit": "500",
     "rnlimit": "500",
    "rnnamespace": "0",
}


nlists = (adjectives, nouns)

hist = []
wordsscanned = 0
results = {}
results_sortiert = {}

async def sample500():
    global hist
    global results
    global wcount
    R = S.get(url=URL, params=PARAMS)
    DATA = R.json()
    corpus = [DATA["query"]["random"][n]["title"] for n in range(len(DATA["query"]["random"])-1)]
    corpus = [c for c in corpus if c not in hist]
    hist += corpus
    html, qcount, wcount = await parallelfetch(url, corpus)
#    html, wcount = await serialfetch(url, corpus)
    scrapeds = await scrape(html, "wiki", "xml", "slot")
    for scraped in scrapeds:
        for w in range(len(scraped)-1):
            if scraped[w] in nlists[0] and bool(scraped[w].strip()):
                try:
                    if scraped[w+1] in nlists[1] and bool(scraped[w+1].strip()):
                        try:
                            results[scraped[w]+" "+scraped[w+1]] += 1
                        except:
                            results[scraped[w]+" "+scraped[w+1]] = 1
                except:
                    pass
    return results, hist, wordsscanned
async def main():
    global wcount
    while True:
        with open("hist.txt", "a") as h:
            h.write(str(hist))
            h.write("\n\n")
        await sample500()
        with open("results.txt", "w") as f:            
            f.write(f"\nparsetheweb 1.0\n\n(c) David Diem 2020 (GPL v3.0)\n\nGiven {len(adjectives)} x {len(nouns)} bigrams\nscanned " + str(len(hist)) + " articles (" + str(wcount) + " words). \n")
        results_sortiert = {k: v for k, v in sorted(results.items(), key= lambda results: results[1], reverse = True)}
        line = 1
        ranksymbol = "#"
        highestrank = 0
        for item in results_sortiert:
            highestrank = list(results_sortiert.values())[0]
            myrank = results_sortiert[item]
            rank = int(myrank/highestrank*10)*ranksymbol
            with open("results.txt", "a") as f:            
                f.write("{:<4}\t{:>3} {:<12}{:^20}\n".format(str(line)+".", str(results_sortiert[item])+"x",rank, item))
            line += 1
#        for bigram, count in results_sortiert.items():
#            print("{:<12}{:<20}\n".format(count, bigram))
#        with open("results.txt", "a") as f:
#            f.writelines("{:<12}{:<20}\n".format(count, bigram) for bigram, count in results_sortiert.items())

loop = asyncio.get_event_loop()
loop.run_until_complete(main())
